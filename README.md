# Image Docker pour le web service d'import pour SyGAL

## Obtention des sources de l'image 

```bash
git clone https://git.unicaen.fr/open-source/docker/sygal-import-ws-image.git
cd sygal-import-ws-image
```

## Construction de l'image (build)

Construisez l'image pour la version de PHP désirée... 

Exemple pour PHP 7.0 :
```bash
PHP_VERSION=7.0 ; \
docker build \
--rm \
--build-arg PHP_VERSION=${PHP_VERSION} \
-t sygal-import-ws-image-php${PHP_VERSION} \
.
```

Si vous êtes derrière un proxy, passez les variables `*_proxy` à la commande `build` avec des `--build-arg` additionnels.

Exemple :
```bash
--build-arg http_proxy=http://proxy.unicaen.fr:3128 \
--build-arg https_proxy=http://proxy.unicaen.fr:3128 \
--build-arg no_proxy=*.unicaen.fr \
```

## Utilisation dans un `docker-compose.yml`

```
version: '2.2'

services:
  sygal:
    image: sygal-import-ws-image-php7.0
    container_name: sygal-import-ws-container-php7.0
    environment:
      - http_proxy
      - https_proxy
      - no_proxy
    ports:
     - "8443:443"
    volumes:
     - .:/app
    working_dir: /app
```

## Utilisation en ligne de commande

*Pré-requis : se placer dans le répertoire contenant les sources du web service.*

- Exemple : démarrage du container pour tester le web service en local à l'adresse `https://localhost:8443`

```bash
docker run \
--rm \
-d \
-p 8443:443 \
--volume ${PWD}:/app \
--name sygal-import-ws-container-php7.0 \
sygal-import-ws-image-php7.0
```

- Exemple : démarrage ponctuel d'un container pour lancer un `composer install`

```bash
docker run \
--rm \
-it \
--volume ${PWD}:/app \
--workdir /app \
sygal-import-ws-image-php7.0 \
composer install
```

- Lancement d'une commande dans un container déjà démarré

```bash
docker exec \
sygal-import-ws-container-php7.0 \
php -v
```
