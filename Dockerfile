###########################################################################################
#
#                    Image Docker pour le web service d'import pour SyGAL.
#
###########################################################################################

ARG PHP_VERSION
ARG OCI8_PACKAGE

FROM unicaen-dev-php${PHP_VERSION}-apache

LABEL maintainer="Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>"

ENV PHP_VERSION=${PHP_VERSION} \
    OCI8_PACKAGE=${OCI8_PACKAGE} \
    APACHE_CONF_DIR=/etc/apache2 \
    PHP_CONF_DIR=/etc/php/${PHP_VERSION}

# Package PHP Oracle OCI8
ADD resources/instantclient-basiclite-linux.x64-18.5.0.0.0dbru.zip /tmp/
ADD resources/instantclient-sdk-linux.x64-18.5.0.0.0dbru.zip /tmp/
ADD resources/instantclient-sqlplus-linux.x64-18.5.0.0.0dbru.zip /tmp/
RUN unzip -o /tmp/instantclient-basiclite-linux.x64-18.5.0.0.0dbru.zip -d /usr/local/ && \
    unzip -o /tmp/instantclient-sdk-linux.x64-18.5.0.0.0dbru.zip -d /usr/local/ && \
    unzip -o /tmp/instantclient-sqlplus-linux.x64-18.5.0.0.0dbru.zip -d /usr/local/ && \
    ln -s /usr/local/instantclient_18_5 /usr/local/instantclient && \
    ln -s /usr/local/instantclient/sqlplus /usr/bin/sqlplus && \
    echo 'instantclient,/usr/local/instantclient' | pecl install ${OCI8_PACKAGE} && \
    echo "extension=oci8.so" > ${PHP_CONF_DIR}/fpm/conf.d/30-php-oci8.ini && \
    echo "extension=oci8.so" > ${PHP_CONF_DIR}/cli/conf.d/30-php-oci8.ini && \
    echo "/usr/local/instantclient" > /etc/ld.so.conf.d/oracle-instantclient.conf && ldconfig

# Symlink apache access and error logs to stdout/stderr so Docker logs shows them
RUN ln -sf /dev/stdout /var/log/apache2/access.log
RUN ln -sf /dev/stdout /var/log/apache2/other_vhosts_access.log
RUN ln -sf /dev/stderr /var/log/apache2/error.log

# Configuration Apache et FPM
ADD docker/apache-ports.conf    ${APACHE_CONF_DIR}/ports.conf
ADD docker/apache-site.conf     ${APACHE_CONF_DIR}/sites-available/app.conf
ADD docker/apache-site-ssl.conf ${APACHE_CONF_DIR}/sites-available/app-ssl.conf
ADD docker/fpm/pool.d/app.conf  ${PHP_CONF_DIR}/fpm/pool.d/app.conf
ADD docker/fpm/conf.d/app.ini   ${PHP_CONF_DIR}/fpm/conf.d/

RUN a2ensite app app-ssl && \
    service php${PHP_VERSION}-fpm reload
