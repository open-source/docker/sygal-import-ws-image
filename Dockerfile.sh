#!/usr/bin/env bash

#
# Script d'install d'un serveur, traduction partielle du Dockerfile.
#

usage() {
  cat << EOF
Script d'install d'un serveur accueillant le web service d'import pour SyGAL, traduction du Dockerfile.
Usage: $0 <version de PHP>
EOF
  exit 0;
}

[[ -z "$1" ]] && usage

################################################################################################################

export PHP_VERSION="$1"
export OCI8_PACKAGE="oci8-2.2.0"
export PHP_CONF_DIR=/etc/php/${PHP_VERSION}

set -e

# Minimum vital
sudo -E apt-get -qq update && \
sudo -E apt-get install -y \
    git \
    nano

# Récupération de l'image Docker Unicaen de base et lancement de son Dockerfile.sh
export UNICAEN_IMAGE_TMP_DIR=/tmp/docker-unicaen-image
git clone https://git.unicaen.fr/open-source/docker/unicaen-image.git ${UNICAEN_IMAGE_TMP_DIR}
cd ${UNICAEN_IMAGE_TMP_DIR}
. Dockerfile.sh ${PHP_VERSION}

# Récupération de l'image Docker de sygal-import-ws pour l'install du driver OCI8
export APP_IMAGE_TMP_DIR=/tmp/docker-sygal-import-ws-image
git clone https://git.unicaen.fr/open-source/docker/sygal-import-ws-image ${APP_IMAGE_TMP_DIR}

cd ${APP_IMAGE_TMP_DIR}
cp resources/instantclient-basiclite-linux.x64-18.5.0.0.0dbru.zip /tmp/
cp resources/instantclient-sdk-linux.x64-18.5.0.0.0dbru.zip /tmp/
cp resources/instantclient-sqlplus-linux.x64-18.5.0.0.0dbru.zip /tmp/
sudo unzip -o /tmp/instantclient-basiclite-linux.x64-18.5.0.0.0dbru.zip -d /usr/local/ && \
sudo unzip -o /tmp/instantclient-sdk-linux.x64-18.5.0.0.0dbru.zip -d /usr/local/ && \
sudo unzip -o /tmp/instantclient-sqlplus-linux.x64-18.5.0.0.0dbru.zip -d /usr/local/ && \
sudo ln -sf /usr/local/instantclient_18_5 /usr/local/instantclient && \
sudo ln -sf /usr/local/instantclient/sqlplus /usr/bin/sqlplus && \
echo 'instantclient,/usr/local/instantclient' | sudo -E pecl install ${OCI8_PACKAGE} && \
sudo sh -c "echo 'extension=oci8.so' > ${PHP_CONF_DIR}/fpm/conf.d/30-php-oci8.ini" && \
sudo sh -c "echo 'extension=oci8.so' > ${PHP_CONF_DIR}/cli/conf.d/30-php-oci8.ini" && \
sudo sh -c "echo '/usr/local/instantclient' > /etc/ld.so.conf.d/oracle-instantclient.conf" && sudo ldconfig

# Configuration Apache et FPM
# (c'est selon le serveur)
